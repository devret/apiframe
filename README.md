# apiframe

#### 介绍
1. 基于Vert.x的轻量api接口框架，利用反射技术实现web接口的快速构建.
2. 通过apiframe接口实现客户端反射jar包中的任意方法
3. 启动快，占用内存小(30M~100M)
4. 支持热更新接口
5. java编写，跨平台
![工作原理](src/main/resources/workfolw.png)
#### 软件架构
1. Vert.x轻量级框架
2. Hutool工具
3. FastJson
4. 反射技术


#### 安装教程
1. 导入Idea
2. Lifecycle: maven clean
3. Lifecycle: maven package

#### 使用说明

1. 将打包生成的apiframe.jar包与start.bat(windows)、start.sh(linux)放同一目录
2. 执行start.bat或者start.sh
3. 浏览器访问`http://ip:18085` 出现success表示接口框架部署完成
4. 如何调用写其它jar包中的方法:支持get或post
    ```
    http://ip:18085/api?class=包名.类名&method=方法名称
    ```
   `http://ip:18085/api?class=com.test.a.Test&method=getTest`
5. 注意：只支持反射一种类型的方法
    ```
    public String getTest(String params) {
       
        return  params;
    }
    ```
6. params参数是apiframe反射时候传入的一个json字符串，包含了地址栏的url上的所有参数，以及一些请求信息
7. 方法返回String会直接显示在浏览器上

