package org.apiframe.utils;

public enum ExceptionEnum {
    方法不存在("NoSuchMethodException", "方法不存在"),
    方法执行异常("InvocationTargetException", "方法执行异常"),
    类不存在("ClassNotFoundException", "类不存在");

    private String value;
    private String name;
    ExceptionEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getName(String value) {
        for (ExceptionEnum e : ExceptionEnum.values()) {
            if (e.getValue().equals(value)) {
                return e.getName();
            }
        }
        return "";
    }

    private String getValue() {
        return value;
    }

    private void setValue(String value) {
        this.value = value;
    }

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }
}
