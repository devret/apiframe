package org.apiframe;

import org.apiframe.utils.ClassLoaderUtils;
import org.apiframe.verticle.ReflectionVerticle;
import io.vertx.core.Vertx;

public class ApiApplication
{
    public static void main(String[] args) {
        //加载插件包
        ClassLoaderUtils.getInstance();
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(ReflectionVerticle.class.getName());
    }

}
