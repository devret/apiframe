@echo off
set filepath=%cd%
set day=%date:~0,4%-%date:~5,2%-%date:~8,2%
set logpath=%filepath%\.vertx\log\ApiFrame-%day%.log
md %filepath%\.vertx\log

%1 mshta vbscript:CreateObject("WScript.Shell").Run("%~s0 ::",0,FALSE)(window.close) &&exit
java -Xms10m -Xmx50m -jar ApiFrame.jar > %logpath% 2>&1 &
exit