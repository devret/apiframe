package org.apiframe.utils;

import com.alibaba.fastjson.JSONObject;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;

public class HttpUtils {

    /**
     * 获取参数
     * @param map
     * @return
     */
    public static JSONObject getParams(MultiMap map){
        JSONObject params = new JSONObject();
        for (String key:map.names()){
            params.put(key,map.get(key));
        }
        return params;
    }

    /**
     * 获取请求头信息
     * @param map
     * @return
     */
    public static JSONObject getHeaders(MultiMap map){
        JSONObject params = new JSONObject();
        for (String key:map.names()){
            params.put(key,map.get(key));
        }
        return params;
    }

    /**
     * 获取请求信息
     * @param request
     * @return
     */
    public static JSONObject getRequest(HttpServerRequest request){
        JSONObject params = new JSONObject();
        //参数
        params.putAll(getParams(request.params()));
        //请求头部信息
        params.put("headers",getHeaders(request.headers()));
        return params;
    }
}
