package org.apiframe.utils;


import cn.hutool.core.lang.JarClassLoader;
import cn.hutool.core.util.ClassLoaderUtil;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassLoaderUtils {

    private static  volatile  ClassLoader instance = null;
    private static int i = 0;
    public final static String path = System.getProperty("user.dir");

    public static ClassLoader getInstance(){
        if(instance==null){
            synchronized (ClassLoader.class){
                if(instance ==null){
                    JarClassLoader jarClassLoader = ClassLoaderUtil.getJarClassLoader(new File(path));
                    URL urls[] = jarClassLoader.getURLs();
                    instance = new URLClassLoader(urls);
                }
            }
        }
        return  instance;
    }

    /**
     *
     * 重载类
     */
    public static void reloadClassLoader(){
        instance = null;
        getInstance();
    }
}
